FROM centos:7

RUN rpm -Uvh https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm && \
    yum update -y && yum -y install openssh-clients puppet-agent
