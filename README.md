# Gitlab CI - Puppet

Docker image based on centos that comes with a Puppet-agent pre-installed. Used to to check puppet modules in CI builds.

## Usage example

```yml
lint:
  image: registry.gitlab.com/thojkooi/ci-puppet
  stage: lint
  script:
    - /opt/puppetlabs/bin/puppet apply --modulepath ./modules manifests/site.pp --noop
```
